from django.urls import path, include
from .views import (
    IndexView,
    EventoCreateView,
    EventoListView,
    EventoUpdateView,
    EventoDeleteView
)


urlpatterns = [

    path('', IndexView.as_view(), name='index'),
    path('eventos/', EventoListView.as_view(), name='eventos'),
    path('eventos/new/', EventoCreateView.as_view(), name='eventos-create'),
    path('eventos/update/<int:pk>', EventoUpdateView.as_view(), name='eventos-update'),
    path('eventos/delete/<int:pk>', EventoDeleteView.as_view(), name='eventos-delete'),
]
