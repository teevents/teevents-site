from django import forms
from bootstrap_datepicker_plus import DatePickerInput
from django.core.mail.message import EmailMessage
from .models import Evento


class CustomEventoForm(forms.ModelForm):

    class Meta:
        model = Evento
        fields = ['nome', 'pessoas', 'evento', 'servico', 'orcamento', 'data']
        widgets = {'data': DatePickerInput(format='%d/%m/%Y')}


class ContactForm(forms.Form):
    nome = forms.CharField(label='Nome', max_length=100, required=True)
    email = forms.EmailField(label='E-mail', max_length=100, required=True)
    assunto = forms.CharField(label='Assunto', max_length=100, required=True)
    mensagem = forms.CharField(label='Mensagem', widget=forms.Textarea(attrs={'rows': 5, 'cols': 5}), required=True)

    def send_mail(self):
        nome = self.cleaned_data['nome']
        email = self.cleaned_data['email']
        assunto = self.cleaned_data['assunto']
        mensagem = self.cleaned_data['mensagem']

        conteudo = f'Nome: {nome}\nE-mail: {email}\nAssunto: {assunto}\nMensagem: {mensagem}'

        mail = EmailMessage(
            subject=assunto,
            body=conteudo,
            from_email='eventseasy2@gmail.com',
            to=['eventseasy2@gmail.com',],
            headers={'Reply-To': email}
        )
        mail.send()
