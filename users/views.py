from django.shortcuts import redirect
from django.contrib import messages
from django.urls import reverse_lazy
from django.contrib.auth.mixins import (
    LoginRequiredMixin,
    UserPassesTestMixin
)
from django.views.generic import (
    CreateView,
    UpdateView,
    TemplateView,
)
from .forms import (
    CustomUserCreateForm,
    CustomUserChangeForm,
    ProfileChangeForm,
)
from .models import CustomUser


class UserCreateView(CreateView):
    model = CustomUser
    form_class = CustomUserCreateForm
    template_name = 'register.html'
    success_url = reverse_lazy('register')

    def form_valid(self, form):
        messages.success(self.request, f'Conta Criada com Sucesso!!')
        return super().form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, 'Não foi possivel Criar a Conta')
        return super(UserCreateView, self).form_invalid(form)


class UserProfileView(LoginRequiredMixin, TemplateView):
    template_name = 'profile.html'


class UserUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = CustomUser
    form_class = CustomUserChangeForm
    second_form_class = ProfileChangeForm
    template_name = 'profile_update.html'
    success_url = reverse_lazy('profile')

    def post(self, request, *args, **kwargs):
        form = CustomUserChangeForm(request.POST, instance=request.user)
        form2 = ProfileChangeForm(request.POST, request.FILES, instance=request.user.profile)
        if form.is_valid() and form2.is_valid():
            form.save()
            form2.save()
            messages.success(request, f'Perfil Atualizado com Sucesso!')
            return redirect('profile')
        else:
            form = CustomUserChangeForm(instance=request.user)
            form2 = ProfileChangeForm(instance=request.user.profile)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'form2' not in context:
            context['form2'] = self.second_form_class(instance=self.request.user.profile)
        return context

    def form_valid(self, form, *args, **kwargs):
        messages.success(self.request, f'Perfil Atualizado com Sucesso!!')
        return super(UserUpdateView, self).form_valid(form, *args, **kwargs)

    def test_func(self):
        customuser = self.get_object()
        if self.request.user.pk == customuser.pk:
            return True

        return False

