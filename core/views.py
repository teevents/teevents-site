from django.contrib.auth.mixins import (
    LoginRequiredMixin,
    UserPassesTestMixin
)
from django.http import HttpResponseRedirect
from django.views.generic import (
    CreateView,
    ListView,
    UpdateView,
    TemplateView,
    DeleteView,
    FormView
)
from django.contrib import messages
from django.urls import reverse_lazy
from .models import Evento, Servico
from .forms import CustomEventoForm, ContactForm
from datetime import datetime

# Create your views here.


class IndexView(FormView):
    template_name = 'index.html'
    form_class = ContactForm
    success_url = reverse_lazy('index')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['servico'] = Servico.objects.order_by('?').all()
        return context

    def form_valid(self, form, *args, **kwargs):
        form.send_mail()
        messages.success(self.request, 'E-mail enviado com sucesso!')
        return super(IndexView, self).form_valid(form, *args, **kwargs)

    def form_invalid(self, form, *args, **kwargs):
        messages.error(self.request, 'Erro ao enviar e-mail!')
        return super(IndexView, self).form_invalid(form, *args, **kwargs)


class EventoListView(LoginRequiredMixin, ListView):
    model = Evento
    template_name = 'eventos.html'
    queryset = Evento.objects.all()
    context_object_name = 'eventos'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['mes'] = datetime.now().strftime('%B')
        return context


class EventoCreateView(LoginRequiredMixin, CreateView):
    model = Evento
    form_class = CustomEventoForm
    template_name = 'eventos_form.html'
    success_url = reverse_lazy('eventos')

    def form_valid(self, form):
        form.instance.autor = self.request.user
        return super().form_valid(form)


class EventoUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Evento
    form_class = CustomEventoForm
    template_name = 'eventos_form.html'
    success_url = reverse_lazy('eventos')

    def form_valid(self, form):
        form.instance.autor = self.request.user
        return super().form_valid(form)

    def test_func(self):
        evento = self.get_object()
        if self.request.user == evento.autor:
            return True
        return False


class EventoDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Evento
    template_name = 'eventos_delete.html'
    success_url = reverse_lazy('eventos')

    def test_func(self):
        evento = self.get_object()
        if self.request.user == evento.autor:
            return True
        return False
