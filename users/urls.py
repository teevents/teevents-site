from django.urls import path
from django.contrib.auth import views as auth_views
from .views import (
    UserCreateView,
    UserProfileView,
    UserUpdateView,
)

urlpatterns = [

    path('register/', UserCreateView.as_view(), name='register'),
    path('login/', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='logout.html'), name='logout'),
    path('profile/', UserProfileView.as_view(), name='profile'),
    path('<int:pk>/profile_update/', UserUpdateView.as_view(), name='profile-update'),
    ]


