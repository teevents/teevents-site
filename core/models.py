from django.db import models

from stdimage.models import StdImageField
from django.contrib.auth import get_user_model
# Create your models here.


class Base(models.Model):
    created_at = models.DateField('Criado', auto_now=True)
    updated_at = models.DateField('Atualizado', auto_now=True)
    ativo = models.BooleanField('Ativo?', default=True)

    class Meta:
        abstract = True


class TipoEvento(Base):
    tipoevento = models.CharField('Tipo Evento', max_length=100)

    class Meta:
        verbose_name = 'Tipo de Evento'
        verbose_name_plural = 'Tipos de Evento'

    def __str__(self):
        return self.tipoevento


class TipoServico(Base):
    tiposervico = models.CharField('Tipo Serviço', max_length=100)

    class Meta:
        verbose_name = 'Tipo de Serviço'
        verbose_name_plural = 'Tipos de Serviço'

    def __str__(self):
        return self.tiposervico


class Evento(Base):
    autor = models.ForeignKey(get_user_model(), verbose_name='Autor', on_delete=models.CASCADE)
    nome = models.CharField('Nome', max_length=100)
    pessoas = models.IntegerField('Quantidade Pessoas')
    evento = models.ForeignKey('core.TipoEvento', verbose_name='Evento', on_delete=models.CASCADE)
    servico = models.ForeignKey('core.TipoServico', verbose_name='Serviço', on_delete=models.CASCADE)
    orcamento = models.DecimalField('Orçamento', decimal_places=2, max_digits=8)
    data = models.DateField('Data', auto_now=False)

    class Meta:
        verbose_name = 'Evento'
        verbose_name_plural = 'Eventos'

    def __str__(self):
        return self.nome


class Servico(Base):
    ICONE_CHOICES = (
        ('fas fa-beer', 'beer'),
        ('fas fa-glass-whiskey', 'glass-whiskey'),
        ('fas fa-home', 'house'),
        ('fas fa-comment-dots', 'comment-dots'),
        ('fas fa-calendar-check', 'calendar-check'),
        ('fas fa-glass-cheers', 'glass-cheers'),
        ('fas fa-users', 'users'),
        ('fas fa-map-marked-alt', 'map-marked'),
    )
    nomeServico = models.CharField('Nome Serviço', max_length=255)
    descricao = models.TextField('Descrição', max_length=255)
    icone = models.CharField('Icone', max_length=50, choices=ICONE_CHOICES)

    class Meta:
        verbose_name = 'Serviço'
        verbose_name_plural = 'Serviços'

    def __str__(self):
        return self.nomeServico

