from django.conf import settings
from django.urls import path, include
from django.contrib import admin
from django.conf.urls.static import static

urlpatterns = [
                  path('admin/', admin.site.urls),
                  path('', include('core.urls')),
                  path('users/', include('users.urls')),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


admin.AdminSite.site_header = '2Easy-Events'
admin.AdminSite.site_title = 'Admin Panel'
admin.AdminSite.index_title = 'Administração 2Easy-Events'