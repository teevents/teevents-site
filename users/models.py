import uuid
from django.db import models
from stdimage.models import StdImageField
from django.contrib.auth.models import (
    AbstractUser,
    BaseUserManager,
)


def get_file_path(_instace, filename):
    ext = filename.split('.')[-1]
    filename = f'{uuid.uuid4()}.{ext}'
    return filename


class UserManager(BaseUserManager):

    use_in_migrations = True

    def _create_user(self, email, username, password, **extra_fields):
        if not email:
            raise ValueError('O E-mail é Obrigatório')
        email = self.normalize_email(email)
        user = self.model(
            email=email,
            username=username,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, username, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, username, password, **extra_fields)

    def create_superuser(self, email, username, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_staff', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser precisa ter is_superuser=True')

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser precisa ter is_staff=True')

        return self._create_user(email, username, password, **extra_fields)


class CustomUser(AbstractUser):
    GENDER_CHOICES = (
        ('Masculino', 'Masculino'),
        ('Feminino', 'Feminino'),
        ('Não Binário', 'Não Binário'),
    )
    email = models.EmailField('E-mail', unique=True)
    first_name = models.CharField('Nome', max_length=50)
    last_name = models.CharField('Sobrenome', max_length=50)
    born = models.DateField('Data de Nascimento', null=True)
    genero = models.CharField('Gênero', max_length=50, choices=GENDER_CHOICES)
    fone = models.CharField('Telefone', max_length=15)
    city = models.CharField('Cidade', max_length=30)
    uf = models.CharField('UF', max_length=30)
    is_staff = models.BooleanField('Membro da Equipe', default=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'fone', 'city', 'uf']

    class Meta:
        verbose_name = 'Usuário'
        verbose_name_plural = 'Usuários'

    def __str__(self):
        return self.email

    objects = UserManager()


class Profile(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    userCompany = models.OneToOneField
    imagem = StdImageField('Imagem', upload_to=get_file_path, default='default.jpg',
                          variations={'thumb': {'width': 150, 'height': 150, 'crop': True}})

    def __str__(self):
        return f'{self.user.username}'
