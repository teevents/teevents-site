from django.contrib import admin
from .models import TipoEvento, TipoServico, Evento, Servico


@admin.register(TipoEvento)
class TipoEventoAdmin(admin.ModelAdmin):
    list_display = ('tipoevento', 'ativo', 'updated_at')


@admin.register(TipoServico)
class TipoServicoAdmin(admin.ModelAdmin):
    list_display = ('tiposervico', 'ativo', 'updated_at')


@admin.register(Evento)
class EventoAdmin(admin.ModelAdmin):
    list_display = ('autor', 'nome', 'pessoas', 'evento', 'servico', 'orcamento', 'data', 'ativo', 'created_at', 'updated_at')
    exclude = ['autor']

    def save_model(self, request, obj, form, change):
        obj.autor = request.user
        super().save_model(request, obj, form, change)

@admin.register(Servico)
class ServicoAdmin(admin.ModelAdmin):
    list_display = ('nomeServico', 'descricao', 'icone', 'created_at', 'updated_at', 'ativo')

